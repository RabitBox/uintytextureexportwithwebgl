# UintyTextureExportWithWebGL 
Unity WebGLからJavaScriptプラグインを経由して画像を保存するプログラムのサンプル。  
適当に書き込んだ後「Export」ボタンを押すと、保存が行える。

## 動作確認
https://rabitbox.gitlab.io/uintytextureexportwithwebgl/

## Environment
- Unity 2020.3.25f1

## How to use
1. Assetフォルダ以下に以下の２つを置く。
   - `ExportTextureJSPlugins.jslib`
   - `ExportTexturePlugin.cs`
2. 保存したいテクスチャ情報をbase64に変換する。
   - 現状はPNG形式にのみ対応
   - 変換方法は`ExportTexture.cs`に記載
3. `ExportTexturePlugin.ExportTexture()`にbase64に変換したテクスチャを渡す。