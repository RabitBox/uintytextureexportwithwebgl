using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class Brush : MonoBehaviour
{
    [SerializeField]
    private Texture2D _refBrushTexture = null;          // Ref Brush Image

    private Texture2D _resizedBrushTexture = null;      // Use Brush Image

    [SerializeField]
    private Color _color = Color.black;                 // Draw Color

    [SerializeField]
    private float _initScale = 1;

    public float BrushScale { get; private set; } = 1.0f;// Brush Scale

    public Color32[] BrushBuffer { get; private set; }  // Brush

	public Vector2 BrushSize { get { return new Vector2(_resizedBrushTexture.width, _resizedBrushTexture.height); } }

	// Start is called before the first frame update
	void Start()
    {
        _resizedBrushTexture = _refBrushTexture;
        ChangeBrushSize(_initScale);
        // CreateBrushBuffer();
    }

    /// <summary>
    /// Create Brush Buffer
    /// </summary>
    private void CreateBrushBuffer()
    {
        // Create Buffer from Texture
        BrushBuffer = _resizedBrushTexture.GetPixels32();// new Color32[ pixels.Length ];

        Debug.Log(BrushBuffer.Length);

		for (int i = 0; i < BrushBuffer.Length; i++)
		{
            // Create Gray Scale
            byte gray = (byte)(BrushBuffer[i].r * 0.2126 + BrushBuffer[i].g * 0.7152 + BrushBuffer[i].b * 0.0722);

            // Set Color
            BrushBuffer[i] = new Color32(
                (byte)(gray * _color.r),
                (byte)(gray * _color.g),
                (byte)(gray * _color.b),
                BrushBuffer[i].a);
        }
    }

    /// <summary>
    /// Change Brush Color
    /// </summary>
    /// <param name="newColor">New Color</param>
    public void ChangeBrushColor(Color newColor)
	{
        _color = newColor;

        // Update Brush Buffer
        CreateBrushBuffer();
    }

    /// <summary>
    /// Change Brush Size
    /// </summary>
    /// <param name="scale">New Scale</param>
    public void ChangeBrushSize( float scale )
	{
		if ( scale < 0.1f )
		{
            return;
		}

		if ( BrushScale != scale )
		{
            // Save Scale
            BrushScale = scale;

            // Update _resizedBrushTexture
            Vector2 newSize = new Vector2(_refBrushTexture.width * scale, _refBrushTexture.height * scale);
            _resizedBrushTexture = ResizeTexture(newSize);
        }
        
        // Update Brush Buffer
        CreateBrushBuffer();
    }

    /// <summary>
    /// Resize Texture
    /// </summary>
    /// <param name="newSize">New Size</param>
    /// <returns>Resized Texture</returns>
    private Texture2D ResizeTexture(Vector2 newSize)
	{
        int newWidth = (int)newSize.x;
        int newHeight = (int)newSize.y;

        var rt = RenderTexture.GetTemporary(newWidth, newHeight);
        Graphics.Blit(_refBrushTexture, rt);

        var preRT = RenderTexture.active;
        RenderTexture.active = rt;
        var ret = new Texture2D(newWidth, newHeight);
        ret.ReadPixels(new Rect(0, 0, newWidth, newHeight), 0, 0);
        ret.Apply();
        RenderTexture.active = preRT;

        RenderTexture.ReleaseTemporary(rt);
        return ret;

        //Texture2D resized = new Texture2D(newWidth, newHeight);
        //Graphics.ConvertTexture(_refBrushTexture, resized);
        //return resized;
    }
}
