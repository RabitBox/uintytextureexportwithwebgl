using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class DrawableTexture
{
    public Texture2D Texture { get; private set; }

    public DrawableTexture(int width, int height) {
        Texture = new Texture2D(width, height, TextureFormat.RGBA32, false);
        WhiteTexture();
    }

    /// <summary>
    /// 
    /// </summary>
    private void WhiteTexture()
    {
        int width = Texture.width;
        int height = Texture.height;

        for (int w = 0; w < width; w++)
        {
            for (int h = 0; h < height; h++)
            {
                Texture.SetPixel(w, h, Color.white);
            }
        }
        Texture.Apply();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <param name="brush"></param>
    public void DrawLine(Vector2 start, Vector2 end, Brush brush)
    {
        var direction = end - start;
        var distance = (int)direction.magnitude;
        direction = direction.normalized;

        if (distance <= 1)
        {   // 距離が1以下なら点を描く
            this.SetPixels(end, brush);
        }
        else
        {   // 2以上ならば線を描く
            // Loop for Distance
            for (int d = 0; d < distance; d++)
            {
                this.SetPixels(start + (direction * d), brush);
            }
        }

        Texture.Apply();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="position"></param>
    /// <param name="brush"></param>
    public void DrawPoint(Vector2 position, Brush brush)
    {
        this.SetPixels(position, brush);
        Texture.Apply();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="position"></param>
    /// <param name="brush"></param>
    public void SetPixels(Vector2 position, Brush brush)
    {
        // Get Brush Infomation
        var size = brush.BrushSize;
        var brushBuffer = brush.BrushBuffer;
        int width = (int)size.x;
        int height = (int)size.y;

        // Get Draw Start Position
        int positionX = (int)(position.x - width / 2.0f);
        int positionY = (int)(position.y - height / 2.0f);

        // Draw
        for (int h = 0; h < height; ++h)
        {
            // Draw Point Y
            int drawY = positionY + h;

            // Check: Out of Canvas
            if (drawY < 0 || drawY >= Texture.height)
            {
                continue;
            }

            for (int w = 0; w < width; ++w)
            {
                // Draw Point X
                int drawX = positionX + w;

                // Check: Inside Canvas
                if (drawX >= 0 && drawX < Texture.width)
                {
                    // Brush Buffer Index
                    int brushIndex = (int)(w + size.x * h);

                    if (brushBuffer[brushIndex].a > 254 && Texture.GetPixel(drawX, drawY).Equals(brushBuffer[brushIndex]) == false)
                    {
                        // Draw
                        Texture.SetPixel(drawX, drawY, brushBuffer[brushIndex]);
                    }
                }
            }
        }
    }
}
