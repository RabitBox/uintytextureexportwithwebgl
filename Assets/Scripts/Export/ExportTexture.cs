using System;
using UnityEngine;
using UnityEngine.UI;

public class ExportTexture : MonoBehaviour
{
	[SerializeField]
	private RawImage _image;

    public void OnPressExport()
	{
		// Textuer2Dをpngに変換した後、base64に変換
		var texture = (Texture2D)_image.texture;
		var png = texture.EncodeToPNG();
		var base64 = Convert.ToBase64String(png);

		// JavaScript経由で保存(WebGL限定)
		ExportTexturePlugin.ExportTexture(base64);
	}
}
