using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[DisallowMultipleComponent]
public class PaintController : MonoBehaviour
{
    // Canvas
    [SerializeField]
    private RawImage _image = null;     // Canvas Image
    [SerializeField]
    private Vector2 _setCanvasSize;
    private Vector2 _canvasScale;

    // Canvas Positions
    private Vector2 _prePosition;   // Previous Position
    private Vector2 _touchPosition; // Now Touch Position
    private Vector2 _disImagePos;   // Displaces Canvas Image Position

    // Input
    private bool _isInput;

    // Brush
    private Brush _brush = null;
    private DrawableTexture _drawable = null;

    // Start is called before the first frame update
    void Start()
    {
        // Get Brush Component
        _brush = GetComponent<Brush>();


        // Init Canvas Image
        _drawable = new DrawableTexture((int)_setCanvasSize.x, (int)_setCanvasSize.y);
        _image.texture = _drawable.Texture;//_texture;

        // Change Anchors
        var rectTransform = _image.gameObject.GetComponent<RectTransform>();
        rectTransform.SetPivotWithKeepingPosition( new Vector2(0.5f, 0.5f) );
        rectTransform.SetAnchorWithKeepingPosition(Vector2.zero, Vector2.zero);

        // Init Displaces Canvas Image Position
        var rect = rectTransform.rect;
        _canvasScale = new Vector2((_setCanvasSize.x / rect.width), (_setCanvasSize.y / rect.height));
        var _imagePos = _image.gameObject.GetComponent<RectTransform>().anchoredPosition;
        _disImagePos = new Vector2(_imagePos.x - rect.width / 2, _imagePos.y - rect.height / 2);
    }

    /// <summary>
    /// On Drag Event
    /// </summary>
    /// <param name="arg">Input Event</param>
	public void OnDrag( BaseEventData arg )
	{
        // Get Input Event
        PointerEventData pointerEvent = arg as PointerEventData;

        // Update Input Datas
        _touchPosition = CalcFixPosition(pointerEvent.position);    // Update Input Canvas Position

        if(_isInput) _drawable.DrawLine(_prePosition, _touchPosition, _brush);

        // Update Previous Input Datas
        _prePosition = _touchPosition;
    }

    /// <summary>
    /// On Tap Event
    /// </summary>
    /// <param name="arg">Input Event</param>
    public void OnTap( BaseEventData arg )
	{
        // Input Flag
        _isInput = true;

        // Get Input Event
        PointerEventData pointerEvent = arg as PointerEventData;

        // Update Input Datas
        _touchPosition = CalcFixPosition(pointerEvent.position);    // Update Input Canvas Position

        _drawable.DrawPoint(_touchPosition, _brush);

        _prePosition = _touchPosition;
    }

    public void OnExit( BaseEventData arg ){
        _isInput = false;
    }

    /// <summary>
    /// Calc Fix Position
    /// </summary>
    /// <param name="position"></param>
    private Vector2 CalcFixPosition( Vector2 position )
	{
        var shiftPosition = position - _disImagePos;
        return Vector2.Scale(shiftPosition, _canvasScale);
	}
}